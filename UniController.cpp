
#define __STDC_LIMIT_MACROS
#include <stdint.h>

#include "UniController.h"

#include <new>
#include <assert.h>
#include <cmath>
#include <cctype>
#include <cstring>

namespace
{

size_t calc_data_size(int numof_buttons, int numof_axis)
{
    assert(numof_buttons > 0);
    assert(numof_axis > 0);

    size_t size = 0;
    size += 2;
    size += ((numof_buttons + 7) / 8);
    size += numof_axis;
    size += 1;

    return size;
}

}

UniController::UniController(PinName tx, PinName rx, int numof_buttons, int numof_axis)
    : Stream(NULL), _serial(tx, rx), _numof_buttons(numof_buttons), _numof_axis(numof_axis)
    , _buttons(new bool[numof_buttons]()), _axis(new uint8_t[numof_axis]())
    , _axis_zero_points(new uint8_t[numof_axis]()), _axis_deadzone(0.1)
    , _timeout_period(0.0), _has_timed_out(true)
    , _data_size(calc_data_size(_numof_buttons, _numof_axis)), _buffer(new uint8_t[_data_size]())
    , _decoder(_buffer, _data_size)
{
    for(int i = 0; i < _numof_axis; i++) {
        _axis_zero_points[i] = UINT8_MAX / 2;
    }
    _serial.baud(115200);
    _serial.attach(callback(this, &UniController::_on_serial_receive));
}

UniController::~UniController()
{
    delete[] _buttons;
    delete[] _axis;
    delete[] _axis_zero_points;
    delete[] _buffer;
}

bool UniController::get_button(int button_id)
{
    if(button_id < _numof_buttons && button_id >= 0) {
        return _buttons[button_id];
    } else {
        return false;
    }
}

double UniController::get_axis(int axis_id)
{
    if(axis_id < _numof_axis && axis_id >= 0) {
        double axis;
        axis = (double)(get_axis_raw(axis_id) - _axis_zero_points[axis_id]) / ((double)UINT8_MAX / 2.1);
        if(axis > 1.0) {
            axis = 1.0;
        } else if(axis < -1.0) {
            axis = -1.0;
        } else if(fabs(axis) < _axis_deadzone) {
            axis = 0.0;
        }
        return axis;
    } else {
        return 0.0;
    }
}

uint8_t UniController::get_axis_raw(int axis_id)
{
    if(axis_id < _numof_axis && axis_id >= 0) {
        return _axis[axis_id];
    } else {
        return 0;
    }
}

void UniController::reset_all_axis()
{
    for(int i = 0; i < _numof_axis; i++) {
        _axis_zero_points[i] = get_axis_raw(i);
    }
}

void UniController::set_axis_deadzone(double dead)
{
    assert(dead >= 0.0);
    _axis_deadzone = dead;
}

void UniController::set_timeout(double period)
{
    assert(period >= 0.0);
    _timeout_period = period;
}

bool UniController::has_timed_out()
{
    return _has_timed_out;
}

int UniController::readable()
{
    return 0;
}

// Stream 用

int UniController::_getc()
{
    // Mutex is already held
    return EOF;
}

int UniController::_putc(int c)
{
    // Mutex is already held
    return _serial.putc(c);
}

void UniController::lock()
{
    _mutex.lock();
}

void UniController::unlock()
{
    _mutex.unlock();
}

// プライベート関数

int UniController::_parse(int length)
{
    if(length != _data_size) {
        return -1;
    }

    uint8_t check_sum = 0xFF;
    for(int i = 0; i < _data_size; i++) {
        check_sum -= _buffer[i];
    }
    if(check_sum != 0) {
        return -2;
    }

    if(_buffer[0] != _numof_buttons) {
        return 1;
    }
    if(_buffer[1] != _numof_axis) {
        return 2;
    }

    const int button_offset = 2;
    const int axis_offset = button_offset + ((_numof_buttons + 7) / 8);

    for(int i = 0; i < _numof_buttons; i++) {
        _buttons[i] = (_buffer[button_offset + i / 8] & (1 << (i % 8))) ? true: false;
    }
    memcpy(_axis, _buffer + axis_offset, _numof_axis);

    _has_timed_out = false;
    _set_timer();

    return 0;
}

void UniController::_on_serial_receive()
{
    while(_serial.readable()) {
        int c = _serial.getc();
        if(c == EOF) {
            break;
        }
        Decoder::DecoderState state = _decoder.decode(c);
        if(state == Decoder::EndOfData) {
            _parse(_decoder.get_length());
        }
    }
}

void UniController::_on_timeout()
{
    _has_timed_out = true;
}

void UniController::_set_timer()
{
    if(_timeout_period > 0.0) {
        _timer.attach(callback(this, &UniController::_on_timeout), _timeout_period);
    }
}

// デコーダ クラス

UniController::Decoder::Decoder(uint8_t *buffer, size_t size)
    : _buffer(buffer), _buffer_size(size), _buffer_tail(0), _decoder_state(-1) {}

//    .------------------------------------------.
//    V   '['                         '.'        |
//  ((0))-------..----------------------.        |
//        '['   VV  xdigit      xdigit  |   ']'  | '\n'
// ->[-1]----->(10)------->(11)------->(12)---->(20)
UniController::Decoder::DecoderState UniController::Decoder::decode(char c)
{
    switch(_decoder_state) {
        case -1:
        case 0:
            _buffer_length = 0;
            _buffer_tail = 0;

            if(c == '[') {
                _decoder_state = 10;
            } else {
                _decoder_state = -1;
            }
            break;

        case 10:
            if(isxdigit(c)) {
                _temp_buff[0] = c;

                _decoder_state = 11;
            } else {
                _decoder_state = -1;
            }
            break;

        case 11:
            if(isxdigit(c)) {
                _temp_buff[1] = c;
                _temp_buff[2] = '\0';
                if(_buffer_tail < _buffer_size) {
                    _buffer[_buffer_tail] = strtol(_temp_buff, NULL, 16);
                }
                _buffer_tail++;

                _decoder_state = 12;
            } else {
                _decoder_state = -1;
            }
            break;

        case 12:
            if(c == '.') {
                _decoder_state = 10;
            } else if(c == ']') {
                _decoder_state = 20;
            } else {
                _decoder_state = -1;
            }
            break;

        case 20:
            if(c == '\n') {
                _buffer_length = _buffer_tail;

                _decoder_state = 0;
            } else {
                _decoder_state = -1;
            }
            break;

        default:
            assert(0 && "Decoder : Invalid State!!");
    }

    if(_decoder_state == 0) {
        return EndOfData;
    } else if(_decoder_state > 0) {
        return Decoding;
    } else {
        return InvalidData;
    }
}

int UniController::Decoder::get_length()
{
    return _buffer_length;
}
