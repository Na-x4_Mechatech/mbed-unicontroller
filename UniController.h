#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "mbed.h"

class UniController : public Stream
{
public:
    UniController(PinName tx, PinName rx, int numof_buttons, int numof_axis);
    virtual ~UniController();

    bool get_button(int button_id);
    double get_axis(int axis_id);
    uint8_t get_axis_raw(int axis_id);

    void reset_all_axis();
    void set_axis_deadzone(double);

    void set_timeout(double);
    bool has_timed_out();

    int readable();

protected:
    virtual int _getc();
    virtual int _putc(int c);
    virtual void lock();
    virtual void unlock();
    PlatformMutex _mutex;

private:
    int _parse(int);
    void _on_serial_receive();
    void _on_timeout();
    void _set_timer();

    RawSerial _serial;
    const int _numof_buttons;
    const int _numof_axis;

    bool *_buttons;
    uint8_t *_axis;

    uint8_t *_axis_zero_points;
    double _axis_deadzone;

    Timeout _timer;
    double _timeout_period;
    bool _has_timed_out;

    const size_t _data_size;
    uint8_t *_buffer;

    class Decoder
    {
    public:
        Decoder(uint8_t *, size_t);

        enum DecoderState {
            EndOfData,
            Decoding,
            InvalidData
        };
        DecoderState decode(char);
        int get_length();

    private:
        uint8_t *_buffer;
        const size_t _buffer_size;
        int _buffer_tail;
        int _buffer_length;

        int _decoder_state;
        char _temp_buff[3];
    };
    Decoder _decoder;
};

#endif
